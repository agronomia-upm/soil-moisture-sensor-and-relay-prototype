
This project is a basic implementation for the "Soil Moisture Device" and for the "Electrovalve Device" used in paper XXXXXXXXX. Both are implemented in the same device for simplicity as it was meant for teaching porpuses.

## Motivation

The main goal is to show the evolution of the project coding: From a simple 10-line program that outputs humidity values to the Serial line ​​to a more complex program able to communicate and send humidity over MQTT protocol to a remote server.

This project contains 4 different subprojects.

1. esp8266-dht22: Takes values for environmental humidity and temperature and outputs to the Serial line.
2. esp8266-soil-moisture: Takes values for soil humidity and outputs to the Serial line.
3. esp8266-soil-moisture-relay: The same that above but with a relay to control a water pump.
4. esp8266-soil-moisture-relay-thingsboard: The same that above plus a remote server with remote logic.

Attending where the control logic is processed, we can find to possibilities:

1. Local processing: In the 3rd example the control logic is processed logicaly.
1. Local processing: In the 3rd example the control logic is processed by the the remote server.

The main wiring scheme is as shown below:

![Wemos D1 fritzing scheme](esp8266-soil-moisture/fritzing-scheme.jpg)

