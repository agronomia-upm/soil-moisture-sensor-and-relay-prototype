
#define RELAY_PIN D1
#define SOIL_MOISTURE_DATA_PIN A0
#define SOIL_MOISTURE_POWER_PIN D2

boolean watering = false;

void setup() {
  Serial.begin(115200);

  pinMode(RELAY_PIN, OUTPUT);

  pinMode(SOIL_MOISTURE_POWER_PIN, OUTPUT);
  digitalWrite(SOIL_MOISTURE_POWER_PIN, LOW);

  digitalWrite(RELAY_PIN, LOW);
}

void loop() {
  int soilHumidity = readSoil();

  Serial.print("Soil humidity = ");
  Serial.println(soilHumidity);

  if (soilHumidity < 150) {
    startWatering();
  }
  else {
    stopWatering();
  }
  
  delay(1000);
}

// =======================================

int readSoil() {
  digitalWrite(SOIL_MOISTURE_POWER_PIN, HIGH);
  delay(10);

  int value = analogRead(SOIL_MOISTURE_DATA_PIN);
  digitalWrite(SOIL_MOISTURE_POWER_PIN, LOW);
  return value;
}

// =======================================

int startWatering() {
  if (! watering) {
    watering = true;
    Serial.println("Start watering");
    digitalWrite(RELAY_PIN, HIGH);
  }
}

// =======================================

int stopWatering() {
  if (watering) {
    watering = false;
    Serial.println("Stop watering");
    digitalWrite(RELAY_PIN, LOW);
  }
}





